import React from "react";
import {Route} from "react-router-dom";

const Layout = ({
    path,
    exact = false,
    element
}) =>{
    return(
        <>
        <Route exact={exact} path={path} element = {element}/>
        </>
    )
}

export default Layout;