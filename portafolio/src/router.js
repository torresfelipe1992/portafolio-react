import React from "react";
import {
    BrowserRouter,
    Routes
  } from "react-router-dom";
import Route  from "./layout";
import {HomeView} from "./views";

const Router = () => {
    return( 
        <BrowserRouter>
            <Routes>
                <Route
                    path="/"
                    element={<HomeView />}
                    exact
                />
            </Routes>
        </BrowserRouter>
    )
}

export default Router;