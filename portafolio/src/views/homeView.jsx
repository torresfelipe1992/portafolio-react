import React from "react";
import { Container } from "react-bootstrap";
import {Navbar} from '../components';

const HomeView = () =>{
return(
    <Container>
        <div className="d-flex flex-column vh-100">
            <Navbar />
        </div>
    </Container>

)
}

export default HomeView;


/**
* HEADER
* BODY Y TODAS LAS OTRAS VISTAS SOLO EXPORTACION
*
* FOOTER
*/