import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap';
// import {
//   BrowserRouter,
//   Routes,
//   Route,
// } from "react-router-dom";
import './scss/app.scss';
import {HomeView} from "./views";

// const App = () =>{
  
//   <BrowserRouter>
//     <Routes>
//       <Route path="/" element={<HomeView/>}>
//       </Route>
//     </Routes>
//   </BrowserRouter>
  
// };

ReactDOM.render(
  <React.StrictMode>
     <HomeView/>
  </React.StrictMode>,
  document.getElementById('root')
);

